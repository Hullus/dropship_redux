//Product reducers

export const FETCH_PRODUCTS = "FETCH_PRODUCTS"
export const SELECT_PRODUCT = "SELECT_PRODUCT"
export const SORT_PRODUCTS = "SORT_PRODUCTS"
export const SEARCH_PRODUCTS = "SEARCH_PRODUCTS"
export const FETCHING = "FETCHING"

//Cart reducers

export const ADD_SUCCESS = "ADD_SUCCESS"
export const ADD_FAIL = "ADD_FAIL"
export const REMOVE_FROM_CART = "REMOVE_FROM_CART"
export const FETCH_CART_LIST = "FETCH_CART_LIST"
export const FETCH_CART_FAIL = "FETCH_CART_FAIL"
export const LOADING_DETAIL = "LOADING_DETAIL"
export const MESSAGE = "MESSAGE"

//User reducers

export const USER_DATA = "USER_DATA"