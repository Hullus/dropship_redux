import {USER_DATA} from "./actiontypes";


export const addUserData = (userData) => {
    return ({
        type: USER_DATA,
        payload: {
            userData,
        }
    })
}