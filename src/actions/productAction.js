import axios from "axios";

import {FETCH_PRODUCTS, SELECT_PRODUCT, SORT_PRODUCTS, SEARCH_PRODUCTS, FETCHING} from "./actiontypes";

export const fetchProducts = () => async (dispatch) => {
    dispatch({type: FETCHING})
    const data = await axios.get("http://18.185.148.165:3000/api/v1/products")
    data.data.data.map(item => {
        item.checked = false;
        item.quantity = 0;
    })
    dispatch({
        type: FETCH_PRODUCTS,
        payload: {
            products: data.data
        }
    })
}
export const selectProduct = (filtered) => {
    return {
        type: SELECT_PRODUCT,
        payload: {
            selectedProducts: filtered
        }
    }
}
export const sortProducts = (sort) => async (dispatch) => {
    dispatch({type: "FETCHING"})
    dispatch({
        type: SORT_PRODUCTS,
        payload: {
            sort,
        }
    })
}
export const searchProducts = (searchInput) => {
    return ({
        type: SEARCH_PRODUCTS,
        payload: {
            searchInput,
        }
    })
}
