import {mainAPI} from "../util/API";
import {
    ADD_FAIL,
    ADD_SUCCESS,
    FETCH_CART_FAIL,
    FETCH_CART_LIST,
    LOADING_DETAIL,
    MESSAGE,
    REMOVE_FROM_CART
} from "./actiontypes";


export const addToCart = (productId, qty) => async (dispatch) => {
    dispatch({
        type: MESSAGE,
    });
    try {
        const cartAPI = mainAPI("api/v1/cart/add");
        const cartList = await cartAPI.post("", {productId, qty});
        dispatch({
            type: ADD_SUCCESS,
            payload: {
                cartList: cartList.data.data.cartItem.items,
            },
        });
        alert("Product added to cart")
    } catch (err) {
        dispatch({
            type: ADD_FAIL,
        });
        alert("Product could not be added to cart")
    }
};
export const removeFromCart = (id) => async (dispatch) => {
    try {
        const cartAPI = mainAPI(`api/v1/cart/remove/${id}`);
        const cartList = await cartAPI.post("");

        dispatch({
            type: REMOVE_FROM_CART,
            payload: {
                cartList: cartList.data.data.cartItem.items,
            },
        });
    } catch (err) {
        console.log(err);
    }
};
export const updateCartList = (id, qty) => async (dispatch) => {
    try {
        const cartAPI = mainAPI(`api/v1/cart/update/${id}`);
        const cartList = await cartAPI.post("", {
            qty,
        });

        dispatch({
            type: FETCH_CART_LIST,
            payload: {
                cartList: cartList.data.data.cartItem.items,
            },
        });
    } catch (err) {
        dispatch({
            type: FETCH_CART_FAIL,
            payload: {
                error: err && err.response && err.response.status,
            },
        });
    }
};
export const getCartList = () => async (dispatch) => {
    dispatch({
        type: LOADING_DETAIL,
    });

    try {
        const cartApi = mainAPI("api/v1/cart");
        const cartList = await cartApi.get();
        dispatch({
            type: FETCH_CART_LIST,
            payload: {
                cartList: cartList.data.data.cartItem.items,
            },
        });
    } catch (err) {
        dispatch({
            type: FETCH_CART_FAIL,
            payload: {
                error: err && err.response && err.response.status,
            },
        });
    }
};