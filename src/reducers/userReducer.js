import {USER_DATA} from "../actions/actiontypes";

const initialState = {
    userData: ""
}

export const userReducer = (state = initialState, action) => {
    switch (action.type){
        case USER_DATA:
            return {
                ...state,
                userData: action.payload.userData
            };
        default:
            return {
                ...state
            };
    }
}