import Sort from "../util/Sort"

import {FETCHING, FETCH_PRODUCTS, SELECT_PRODUCT, SORT_PRODUCTS, SEARCH_PRODUCTS} from "../actions/actiontypes";

const initialState = {
    products: [],
    productsShadow: [],
    sortedProducts:[],
    isFetching : true,
    selectedProducts : [],
}

export const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS:
            return {
                ...state,
                products: action.payload.products.data,
                sortedProducts: action.payload.products.data,
                productsShadow: action.payload.products.data,
                isFetching: false
            };
        case SELECT_PRODUCT:
            return{
                ...state,
                selectedProducts: action.payload.selectedProducts
            }
        case SORT_PRODUCTS:
            return{
                ...state,
                products: Sort(action.payload.sort, state.sortedProducts),
                productsShadow: Sort(action.payload.sort, state.products),
                sortedProducts: Sort(action.payload.sort, state.sortedProducts),
                isFetching: false
            }
        case SEARCH_PRODUCTS:
            const Filtered = [...state.productsShadow].filter(data => data.title.toLowerCase().includes(action.payload.searchInput))
            return {
                ...state,
                sortedProducts: Filtered
            }
        case FETCHING:
            return{
                ...state,
                isFetching: true
            }
        default:
            return {...state}
    }
}