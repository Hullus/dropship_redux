import {
    ADD_FAIL,
    ADD_SUCCESS,
    FETCH_CART_FAIL,
    FETCH_CART_LIST,
    LOADING_DETAIL, MESSAGE,
    REMOVE_FROM_CART
} from "../actions/actiontypes";

const initialState = {
    cartProducts: [],
    isLoading: true,
    errorMessage: "",
    message: "",
};

export const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CART_LIST:
            return {
                ...state,
                cartProducts: action.payload.cartList,
                errorMessage: "",
                isLoading: false,
            };

        case FETCH_CART_FAIL:
            return {
                ...state,
                errorMessage: action.payload.error,
                isLoading: false,
            };

        case ADD_SUCCESS:
            return {
                ...state,
                cartProducts: action.payload.cartList,
                message: "DAEMATA DAEMATA"
            };

        case ADD_FAIL:
            return {
                ...state,
                message: "EG RAT GINDA"
            };

        case REMOVE_FROM_CART:
            return {
                ...state,
                cartProducts: action.payload.cartList
            };

        case LOADING_DETAIL:
            return {
                ...state,
                isLoading: true
            };


        case MESSAGE:
            return {
                ...state,
                message: ""
            };


        default:
            return {
                ...state
            };
    }
};

export default cartReducer;
