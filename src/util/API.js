import axios from "axios";

export const SERVER_URL = "http://18.185.148.165:3000/";
export const SERVER_URL_V1 = "http://18.185.148.165:3000/api/v1";


export const handleLogin = async (email, password) => {
    try {
        const res = await axios.post(SERVER_URL + "login", {email, password,});
        localStorage.setItem("user", JSON.stringify(res.data.data));
        localStorage.setItem("token", res.data.data.token);

    } catch (err) {
        console.log(err)
    }
};


export const handleSignUp = async (
    firstName,
    lastName,
    email,
    password,
    passwordConfirmation
) => {
    try {
        const res = await axios.post(SERVER_URL + "register", {
            firstName,
            lastName,
            email,
            password,
            passwordConfirmation,
        });
        localStorage.setItem("user", JSON.stringify(res.data.data));
        localStorage.setItem("token", res.data.data.token);
    } catch (err) {
        throw new Error(err);
    }
};


export const mainAPI = (Link) => {
    return axios.create({
        baseURL: `${SERVER_URL}${Link}`,
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token"),
        },
    });
};