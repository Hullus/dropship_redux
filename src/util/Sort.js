export default function Sort(sort, products) {
    if (sort === "asc") {
        return products.sort((a, b) => b.price - a.price);
    } else if (sort === "desc") {
        return products.sort((a, b) => a.price - b.price);
    } else if (sort === "alphabeticalAscending") {
        return products.sort((a, b) => a.title.localeCompare(b.title));
    } else if (sort === "alphabeticalDescending") {
        return products.sort((a, b) => b.title.localeCompare(a.title));
    } else if (sort === "def") {
        return products;
    }
}
