import {mainAPI} from "./API";

export const fetchSingleProduct = async (id) =>{
    try {
        const product = await mainAPI(`api/v1/products/${id}`).get("")
        return product.data.data
    } catch (err){
        console.log(err)
    }
}

export const addProduct = async (data) => {
    try {
        await mainAPI(`api/v1/products`).post("", {
            title: data.title,
            description: data.description,
            price: data.price,
            imageUrl: data.imageUrl,

        });
        alert("Product created")
    } catch (err) {
        alert("Product could not be added to cart")
    }
};
export const deleteProduct = async (id) => {
    try {
        await mainAPI(`api/v1/products`).delete(`${id}`)
        alert("Product deleted")
    } catch (err) {
        console.log(err)
        alert("Product deleted")
    }
};
export const editProduct = async (data, id) => {
    try {
        await mainAPI(`api/v1/products/${id}`).put("", {
            title: data.title,
            description: data.description,
            price: data.price,
            imageUrl: data.imageUrl,

        });
        alert("Product created")
    } catch (err) {
        alert("Product could not be added to cart")
    }
}

