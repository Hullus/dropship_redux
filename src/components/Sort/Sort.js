import "./Sort.css"
import {useDispatch} from "react-redux";
import {sortProducts} from "../../actions/productAction";


function SortHeader() {

    const dispatch = useDispatch()


    return (
        <div className="Sort">
            <select id="sort" className={"Sort__select"} onChange={(e) => dispatch(sortProducts(e.target.value))}>
                <option value="def">Sort By: New Arrivals</option>
                <option value="asc">Price: High To Low</option>
                <option value="desc">Price: Low To High</option>
                <option value="alphabeticalAscending">Alph: A to Z</option>
                <option value="alphabeticalDescending">Alph: Z to A</option>
            </select>
        </div>
    );
}

export default SortHeader