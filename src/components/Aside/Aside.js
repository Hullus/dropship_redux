import './Aside.css';

import logo from "../../assets/dropship__logo.png"
import {Dashboard, Cart, Catalog, Inventory, Transactions, Orders, Store} from "../../assets/icons";

import {NavLink, Link} from "react-router-dom";

const asideIcons = [
    {
        Svg: Dashboard,
        path: "dashboard",
        className: 'Aside__nav__icon'
    },
    {
        Svg:Catalog,
        path: "catalog",
        className: 'Aside__nav__icon',
    },
    {
        Svg:Inventory,
        path: "inventory",
        className:  'Aside__nav__icon',
    },
    {
        Svg:Cart,
        path: "cart",
        className: 'Aside__nav__icon',
    },
    {
        Svg: Orders,
        path: "orders",
        className: 'Aside__nav__icon',
    },
    {
        Svg:Transactions,
        path: "transactions",
        className: 'Aside__nav__icon',
    },
    {
        Svg: Store,
        path: "store",
        className: 'Aside__nav__icon',
    }

]

function Aside() {
    return (
        <div className="Aside">
            <div className={"Aside__logo"}>
                <Link to={"/"}>
                <img src={logo} alt={"dropShip__logo"} className={"Aside__logo__icon"}/>
                </Link>
            </div>
            <div className={"Aside__nav"}>
                {
                    asideIcons.map((item) => (
                        <NavLink to={`/${item.path}`} className={item.className} activeClassName={'Icon__active'} key={item.path}>
                            <item.Svg fill={"#5d6b9f"} />
                         </NavLink>
                    ))}
            </div>
        </div>
    );
}

export default Aside;