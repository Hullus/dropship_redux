import React from 'react';
import AdminPanelHeader from "../AdminPanelHeader/AdminPanelHeader";
import AdminPanelBody from "../AdminBody/AdminPanelBody";

const AdminProfile = () => {
    return (
        <div>
            <AdminPanelHeader/>
            <AdminPanelBody />
        </div>
    );
};

export default AdminProfile;
