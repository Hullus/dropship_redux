import './Header.css';
import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {searchProducts, selectProduct} from "../../actions/productAction";
import AddToPhotosIcon from '@material-ui/icons/AddToPhotos';
import ProductCreate from "../ProductFactory/ProductCreate/ProductCreate";
import Modal from '@material-ui/core/Modal';

function Header() {

    const products = useSelector((state) => state.products.sortedProducts)
    const Loading = useSelector((state) => state.products.isFetching)
    const selectedProducts = useSelector((state) => state.products.selectedProducts)
    const user = useSelector((state) => state.user.userData)
    const [open, setOpen] = useState(false);

    const dispatch = useDispatch()

    const handleOpen = () => {
        setOpen(true)
    }
    const handleClose = () => {
        setOpen(false)
    }

    const selectAll = () => {
        dispatch(selectProduct(products.map(selected => selected.id)));
        products.map(item => item.checked = true)
    }
    const clearAll = () => {
        dispatch(selectProduct([]));
        products.map(item => item.checked = false)
    }

    return (
        <header className="Header">
            <div className={"Header__nav"}>
                <div className={"Nav__count"}>
                    <button
                        className={"Nav__button Blue__button"}
                        onClick={() => selectAll()}
                    >
                        SELECT ALL
                    </button>

                    <span
                        className={"Nav__count"}>{selectedProducts.length} selected  out of {Loading ? null : products.length} products</span>
                    {selectedProducts.length > 0 && < button
                        className={"Nav__button Blue__button"}
                        onClick={clearAll}
                    >CLEAR SELECTED</button>}
                </div>
                <div className={"Nav__search"}>
                    {user.isAdmin && <AddToPhotosIcon className={"add__product--icon"} onClick={handleOpen}/>}
                    <input
                        className={"Search__input"}
                        type="text"
                        placeholder={"Search..."}
                        onChange={(e) => dispatch(searchProducts(e.target.value))}
                    />
                    <button
                        className={"Nav__button Blue__button"}
                    >
                        ADD TO INVENTORY
                    </button>
                </div>
            </div>
            <Modal
                open={open}
                onClose={handleClose}
                className={"create__product__wrapper"}
            >
                <ProductCreate/>
            </Modal>

        </header>
    );
}

export default Header;