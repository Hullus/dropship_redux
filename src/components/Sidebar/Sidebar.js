import './Sidebar.css';

import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import {Slider} from "@material-ui/core";

function Sidebar() {
    return (
        <div className="Sidebar">
            <div className={"Sidebar__niche Sidebar__tab"}>
                <h4>Choose Niche</h4>
                <KeyboardArrowDownIcon/>
            </div>
            <div className={"Sidebar__category Sidebar__tab"}>
                <div className={"Sidebar__category--title"}
                >
                    <h4>Choose Category</h4>
                    <KeyboardArrowDownIcon/>
                </div>
            </div>
            <div className={"Sidebar__select"}>
                <div className={"Sidebar__select-item"}>
                    <span>ship form</span>
                    <KeyboardArrowDownIcon/>
                </div>
                <div className={"Sidebar__select-item"}>
                    <span>ship to</span>
                    <KeyboardArrowDownIcon/>
                </div>
                <div className={"Sidebar__select-item"}>
                    <span>select supplier</span>
                    <KeyboardArrowDownIcon/>
                </div>
            </div>
            <div className={"Sidebar__filter"}>
                <div className={"Sidebar__slider"}>
                    <div className={"slider__comp--wrapper"}>
                        <p className={"Sidebar__slider-name"}>
                            price range
                        </p>
                        <Slider
                            valueLabelDisplay="auto"
                            aria-labelledby="range-slider"
                            defaultValue={[0, 100]}
                            className={'Sidebar__slider--comp'}
                        />
                    </div>
                </div>
                <div className={"Sidebar__slider"}>
                    <div className={"slider__comp--wrapper"}>
                        <p className={"Sidebar__slider-name"}>
                            profit range
                        </p>
                        <Slider
                            valueLabelDisplay="auto"
                            aria-labelledby="range-slider"
                            defaultValue={[0, 100]}
                            className={'Sidebar__slider--comp'}
                        />
                    </div>
                </div>
                <button className={"Sidebar__reset--slider"}>
                    reset filter
                </button>
            </div>
        </div>
    );
}

export default Sidebar;