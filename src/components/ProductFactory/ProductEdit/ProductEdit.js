import Logo from "../../../assets/dropship__logo.png"

import React from 'react';
import {Formik, Form, Field} from "formik";
import {editProduct} from "../../../util/ProductFactoryAPI";

const ProductEdit = ({data, id}) => {
    return (
        <div className={"product__create"}>
            <img src={Logo} alt="Logo" className={"product__create--logo"}/>
            <Formik
                initialValues={{
                    title: data.title,
                    description: data.description,
                    price: data.price,
                    imageUrl: data.imageUrl,
                }}
                onSubmit={(values, {setSubmitting}) => {
                    setTimeout(() => {
                        editProduct(values, data.id)
                            .then(alert("Product changed"))
                        setSubmitting(false);

                    }, 400);
                }
                }
            >
                {({
                      values,
                      handleSubmit,
                      isSubmitting,
                  }) => (
                    <Form onSubmit={handleSubmit} className={"product__create__form"}>
                        <Field
                            placeholder={"Title"}
                            className={"product__add__field"}
                            name="title"
                            value={values.title}
                        />
                        <Field
                            placeholder={"Description"}
                            className={"product__add__description"}
                            name="description"
                            value={values.description}
                        />
                        <Field
                            placeholder={"Price"}
                            className={"product__add__field"}
                            name="price"
                            value={values.price}
                        />
                        <Field
                            placeholder={"Image url"}
                            className={"product__add__field"}
                            name="imageUrl"
                            value={values.imageUrl}
                        />
                        <button className={"create__product--button"}
                                type="submit"
                                onSubmit={handleSubmit}
                                disabled={isSubmitting}
                        >
                            edit product
                        </button>
                    </Form>)

                }
            </Formik>
        </div>
    );
};

export default ProductEdit;
