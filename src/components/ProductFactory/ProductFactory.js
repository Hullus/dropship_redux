import React from 'react';
import ProductCreate from "./ProductCreate/ProductCreate";
import ProductEdit from "./ProductEdit/ProductEdit";
import ProductDelete from "./ProductDelete/ProductDelete";

const ProductFactory = () => {
    return (
        <>
            <ProductCreate />
            <ProductEdit />
            <ProductDelete />
        </>
    );
};

export default ProductFactory;
