import "./ProductCreate.css"
import Logo from "../../../assets/dropship__logo.png"

import React from 'react';
import {Formik, Form, Field} from "formik";
import {addProduct} from "../../../util/ProductFactoryAPI";


const ProductCreate = () => {
    return (
        <div className={"product__create"}>
            <img src={Logo} alt="Logo" className={"product__create--logo"}/>
            <Formik
                initialValues={{
                    title: '',
                    description: '',
                    price: '',
                    imageUrl: ''
                }}
                onSubmit={(values, {setSubmitting}) => {
                    setTimeout(() => {
                        addProduct(values)
                            .then(alert("daemata"))
                        setSubmitting(false);

                    }, 400);
                }
                }
            >
                {({
                      values,
                      handleSubmit,
                      isSubmitting,
                  }) => (
                    <Form onSubmit={handleSubmit} className={"product__create__form"}>
                        <Field
                            placeholder={"Title"}
                            className={"product__add__field"}
                            name="title"
                            value={values.title}
                        />
                        <Field
                            placeholder={"Description"}
                            className={"product__add__description"}
                            name="description"
                            value={values.description}
                        />
                        <Field
                            placeholder={"Price"}
                            className={"product__add__field"}
                            name="price"
                            value={values.price}
                        />
                        <Field
                            placeholder={"Image url"}
                            className={"product__add__field"}
                            name="imageUrl"
                            value={values.imageUrl}
                        />
                        <button className={"create__product--button"}
                                type="submit"
                                onSubmit={handleSubmit}
                                disabled={isSubmitting}
                        >
                            create product
                        </button>
                    </Form>)

                }
            </Formik>
        </div>
    );
};

export default ProductCreate;
