import React, {useEffect, useState} from 'react';

import "./productDelete.css"
import {Modal} from "@material-ui/core";
import {deleteProduct} from "../../../util/ProductFactoryAPI";

const ProductDelete = (deleteID) => {
    const [open, setOpen] = useState(false)

    const handleClose = () =>{
        setOpen(false)
    }
    useEffect(() => {
        deleteID &&
        setOpen(true)
    }, [deleteID])
    const handleDelete = async () => {
        await deleteProduct(deleteID)
    }
    return (
        <Modal
            open={open}
            onClose={handleClose}
        >
            <div className={"ProductDelete"}>
                <div className={"ProductDelete__title"}>
                    <h3 className={"ProductDelete__text"}>Are you sure you want to <br/> delete this item</h3>
                </div>
                <div className={"ProductDelete__buttons"}>
                    <button className={"ProductDelete__yes"} onClick={handleDelete}>Yes</button>
                    <button className={"ProductDelete__no"}>No</button>
                </div>
            </div>
        </Modal>
    );
};

export default ProductDelete;
