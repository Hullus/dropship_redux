import React from 'react';
import "./AdminPanelHeader.css"

const AdminPanelHeader = () => {
    return (
        <div className="Admin__header">
            <h2 className="Admin__title">my profile</h2>
            <button className="Admin__title--button">sign out</button>
        </div>
    );
};

export default AdminPanelHeader;
