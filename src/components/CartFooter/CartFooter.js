import "./CartFooter.css"

import React, {useState, useEffect} from 'react';

import {useSelector} from "react-redux";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const CartFooter = () => {

    const cartProducts = useSelector((state) => state.cart.cartProducts)
    const [totalPrice, setTotalPrice] = useState(0);
    const [totalQuantity, setTotalQuantity] = useState(0);


    useEffect(() => {
        let total = 0;
        let quantity = 0;
        cartProducts.map(item => {
            total += item.price * item.qty
            quantity += item.qty
        })
         setTotalPrice(total)
         setTotalQuantity(quantity)
    }, [cartProducts]);



    return (
        <div className="cart__footer">
            <div className={"cart__footer--continue"}>
                <button className={"footer--button"}>
                    <ArrowBackIcon className={"footer__continue--arrow"}/>
                    continue shopping
                </button>
            </div>
            <div className={"cart__footer--checkout"}>
                <div className={"footer__checkout--total"}>
                    ORDER QUANTITY: <span className={"checkout__price"}>{totalQuantity}</span>
                </div>
                <div className={"footer__checkout--total"}>
                    ORDER TOTAL: <span className={"checkout__price"}>${totalPrice}</span>
                </div>
                <button className={"footer--button"}>
                    checkout
                </button>
            </div>
        </div>
    );
};

export default CartFooter;
