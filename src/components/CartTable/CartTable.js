import "./CartTable.css"

import React, {useEffect} from 'react';


import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import DeleteIcon from '@material-ui/icons/Delete';

import {useSelector, useDispatch} from "react-redux";
import {getCartList, removeFromCart} from "../../actions/cartAction";
import CartFooter from "../CartFooter/CartFooter";

const CartTable = () => {

    const dispatch = useDispatch()

    const cartProducts = useSelector((state) => state.cart.cartProducts)

    useEffect(() => {
        dispatch(getCartList());
    }, [dispatch]);


    return (
        <div className={"cart__table--wrapper"}>
            <TableContainer component={Paper} className={"cart__table--container"}>
                <Table stickyHeader className={"cart__table"} aria-label="simple table">
                    <TableHead className={"cart__table--header"}>
                        <TableRow>
                            <TableCell>PRODUCT IMAGE</TableCell>
                            <TableCell className={"table__header--cell"}>ITEM TITLE</TableCell>
                            <TableCell className={"table__header--cell"} align="left">QUANTITY</TableCell>
                            <TableCell className={"table__header--cell"} align="left">ITEM PRICE</TableCell>
                            <TableCell className={"table__header--cell"} align="left">TOTAL COST</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {cartProducts.map((row) => (
                            <TableRow key={row.title}>
                                <TableCell>
                                    {<img src={row.image} alt={"SANDROS PIPIEBI"} className={"cart__cell--image"}/>}
                                </TableCell>
                                <TableCell component="th" scope="row">
                                    {row.title}
                                </TableCell>
                                <TableCell align="left">{row.qty}</TableCell>
                                <TableCell align="left">$ {row.price}</TableCell>
                                <TableCell align="left">$ {row.price * row.qty}</TableCell>
                                <TableCell align="center">
                                    <DeleteIcon
                                        className={"cart__delete--icon"}
                                        onClick={() => dispatch(removeFromCart(row.id))}
                                    />
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <CartFooter/>
        </div>
    );
};

export default CartTable;
