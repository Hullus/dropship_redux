import "./CartHeader.css"

import React from 'react';
import {useSelector} from "react-redux";

const CarHeader = () => {
    const cartProducts = useSelector((state) => state.cart.cartProducts)

    return (
        <div className={"cart__header"}>
            <span className={"cart__header--title"}>SHOPPING CART ({cartProducts && cartProducts.length})</span>
        </div>
    );
};

export default CarHeader;
