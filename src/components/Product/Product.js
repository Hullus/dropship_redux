import './Product.css';
import {Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {selectProduct} from "../../actions/productAction";
import {deleteProduct, fetchSingleProduct} from "../../util/ProductFactoryAPI";
import ModalMain from "../Modal/Modal";
import {useState} from "react";
import {addToCart} from "../../actions/cartAction";
import DeleteIcon from '@material-ui/icons/Delete';
import {Modal} from "@material-ui/core";
import EditIcon from '@material-ui/icons/Edit';
import ProductEdit from "../ProductFactory/ProductEdit/ProductEdit";


function Product() {

    const [modalID, setModalID] = useState();
    const [editOpen, setEditOpen] = useState(false);
    const [editProduct, setEditProduct] = useState()

    const dispatch = useDispatch();

    const products = useSelector((state) => state.products.sortedProducts);
    const Loading = useSelector((state) => state.products.isFetching);
    const selectedProducts = useSelector((state) => state.products.selectedProducts);
    const user = useSelector((state) => state.user.userData)

    //Selecting items
    const handleSelect = (item) => {
        if (selectedProducts.includes(item.id)) {
            const filteredProducts = selectedProducts.filter(selected => selected !== item.id)
            dispatch(selectProduct(filteredProducts))
            products.map(product => {
                if (product.id === item.id) item.checked = false
            })
        } else if (!selectedProducts.includes(item.id)) {
            dispatch(selectProduct([...selectedProducts, item.id]));
            products.map(product => {
                if (product.id === item.id) item.checked = true
            })
        }
    }
    //Modal
    const handleClick = (id) => {
        setModalID(id);
    }
    const handleEditOpen = async (id) => {
        const product = await fetchSingleProduct(id)
        setEditProduct(product)
        setEditOpen(true)
    }
    const handleEditClose = () => {
        setEditOpen(false)
    }


    return (
        <>
        <ModalMain
            open={handleClick}
            modalID={modalID}
            className={"Modal"}
        />
        <Modal
            open = {editOpen}
            onClose = {handleEditClose}
            className={"Modal"}
        >
            <ProductEdit data={editProduct} />
        </Modal>


        {Loading ? null :
        products.map(item => {
            return (

                <Grid
                    key={item.id}
                    item
                    xs={12} sm={6} md={6} lg={4}
                >
                    <div
                        className={`Product ${selectedProducts.includes(item.id) ? "Product__hover--active" : ""}`}
                    >
                        <div
                            className={
                                `Product__hover ${selectedProducts.includes(item.id) ? "Product__hover--active" : ""}`}
                        >
                            <input
                                type={"checkbox"}
                                className={"Product__checkbox"}
                                onChange={() => handleSelect(item)}
                                checked={item.checked}
                            />
                            {user.isAdmin &&
                            <EditIcon
                                className={"product__edit"}
                                onClick={() => handleEditOpen(item.id)}
                            />}
                            {user.isAdmin &&
                            <DeleteIcon
                                className={"product__delete"}
                                onClick={() => deleteProduct(item.id)}
                            />}
                            <button onClick={() => dispatch(addToCart(item.id, 1))} >Add to Inventory</button>
                        </div>
                        <div className={"Product__profile"}>
                            <img
                                src={item.imageUrl}
                                className={"Product__image"}
                                alt="Product"
                                onClick={() => handleClick(item.id)}
                            />
                        </div>
                        <div className={"Product__name"}>
                            <p className={"Product__name__text"} >{item.title}</p>
                        </div>
                        <div className={"Product__supplier"}>
                            <p>by: <span className={"Product__category"}>PL-Supplier149</span></p>
                        </div>
                        <div className={"Product__prices"}>
                            <ul className={"Prices__list"}>
                                <li><span className={"Price__number"}>${item.price}  </span><span
                                    className={"Price__text"}>RRP</span></li>
                                <li><span className={"Price__number"}>$25</span> <span
                                    className={"Price__text"}>COST</span>
                                </li>
                                <li><span className={"Price__number"}>69%</span> <span
                                    className={"Price__text--blue"}>PROFIT</span></li>
                            </ul>
                        </div>
                    </div>
                </Grid>
                )
            })}
         </>)
}

export default Product;