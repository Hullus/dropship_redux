import './App.css';
import {useEffect} from "react";
import {useDispatch} from "react-redux";
import {fetchProducts} from "./actions/productAction";
import CatalogMain from "./pages/catalog/CatalogMain";
import Landing from "./pages/Landing/Landing";
import Cart from "./pages/cart/Cart";
import { Route, Switch } from "react-router-dom";
import AdminPanel from "./pages/AdminPanel/AdminPanel";
import ProductDelete from "./components/ProductFactory/ProductDelete/ProductDelete";


function App() {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(fetchProducts());
    }, [dispatch]);

    return (
        <>
            <Switch>
                <Route exact path={"/"}>
                    <Landing />
                </Route>
                <Route path={"/catalog"}>
                    <CatalogMain />
                </Route>
                <Route path={"/cart"}>
                    <Cart />
                </Route>
                <Route path={"/store"}>
                    <AdminPanel />
                </Route>
            </Switch>
        </>
    )
}

export default App;
