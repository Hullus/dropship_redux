import './Catalog.css';

import Product from "../../components/Product/Product";
import {CircularProgress, Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {addUserData} from "../../actions/userAction";


function Catalog() {

    const dispatch = useDispatch();

    useEffect(()=> {
        dispatch(addUserData(JSON.parse(localStorage.getItem("user"))))
    },[])

    const Loading = useSelector((state) => state.products.isFetching)

    return (
        <div>
            {
                Loading ?
                    <div className={"Progress__bar--wrapper"}>
                        <CircularProgress
                            className={"Progress_bar"}
                            size={55}
                            thickness={3}
                        />
                    </div>
                    :
                    <Grid
                        container
                        className="Catalog">
                            <Product/>
                    </Grid>
            }
        </div>
    );
}

export default Catalog;