import React from 'react';
import Aside from "../../components/Aside/Aside";
import Sidebar from "../../components/Sidebar/Sidebar";
import Header from "../../components/Header/Header";
import Sort from "../../components/Sort/Sort";
import Catalog from "./Catalog";

const CatalogMain = () => {
    return (
        <div>
            <Aside />
            <Sidebar />
            <Header />
            <Sort />
            <Catalog />
        </div>
    );
};

export default CatalogMain;
