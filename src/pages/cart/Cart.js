import React from 'react';
import Aside from "../../components/Aside/Aside";
import CartTable from "../../components/CartTable/CartTable";
import CartHeader from "../../components/CartHeader/CartHeader";
import CartFooter from "../../components/CartFooter/CartFooter";

const Cart = () => {
    return (
        <div>
            <Aside/>
            <CartHeader />
            <CartTable />
        </div>
    );
};

export default Cart;
