import "./Landing.css"

import logo from "../../assets/group-30.png"

import React, {useState} from 'react';
import {Modal} from "@material-ui/core";
import Authentication from "./Authentication";

const Landing = () => {

    const [authOpen, setAuthOpen] = useState(false);
    const [signUpOn, setSignUpOn] = useState();

    const handleClose = () => {
        setAuthOpen(false);
    }
    const handleModalLogIn = () => {
        setSignUpOn(true);
        setAuthOpen(true);
    }
    const handleModalSignUp = () => {
        setSignUpOn(false);
        setAuthOpen(true);
    }


    return (
        <div className={"Landing"}>
            <Modal
                className={"Authentication__modal"}
                open={authOpen}
                onClose={handleClose}
            >
                <Authentication
                    signUpOn={signUpOn}
                    setSignUpOn={setSignUpOn}
                />
            </Modal>


            <div>
                <div className={"Landing__header"}>
                    <div className="Header__logo">
                        <img src={logo}/>
                    </div>
                    <div className={"landing__Header__nav"}>
                        <ul className={"landing__nav--list"}>
                            <li>about</li>
                            <li>catalog</li>
                            <li>pricing</li>
                            <li>suppliers</li>
                            <li>help center</li>
                            <li>blog</li>
                        </ul>
                        <button className={"Landing__sign-up"} onClick={handleModalSignUp}>sign up now</button>
                        <button className={"Landing__login"} onClick={handleModalLogIn}>login</button>
                    </div>
                </div>
                <div className={"Landing__body"}>
                    <div className={"Banner"}>
                        <img
                            className={"Banner__logo"}
                            src={"https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/356Logo.svg"}
                            alt={"logo"}/>
                        <h4>WE GOT YOUR SUPPLY CHAIN COVERED</h4>
                        <h4>365 DAYS A YEAR!</h4>
                    </div>
                    <button
                        className={"Landing__body--sing-up"}
                        onClick={handleModalSignUp}
                    >sign up now
                    </button>
                </div>
            </div>
        </div>
    );
};

export default Landing;
