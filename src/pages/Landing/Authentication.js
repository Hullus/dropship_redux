import "./Authentication.css"

import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';

import React from 'react';
import {handleLogin, handleSignUp} from "../../util/API";
import {Formik, Form, Field} from "formik";
import {useHistory} from "react-router-dom";
import * as yup from "yup";

const Authentication = ({signUpOn, setSignUpOn}) => {

    const history = useHistory();
    const loginValidation = yup.object({
        // email: yup
        //     .string()
        //     .email("Tis not AN EMAIL ADDRESS")
        //     .required("TIS NEED BE COMPLETED FAIR KNIGHT"),
        // password: yup
        //     .string()
        //     .required("TIS NEED BE LEST 7 CHARACTERS LONG FAIR KNIGHT")
        //     .min(4, "moklea")
        //     .max(25, "nametania")
    })


    return (

        <>
            {
                signUpOn ?

                    <div className={"Login"}>
                        <div className={"Login__title"}>
                            <img src={"https://app.365dropship.com/assets/images/auth/logo.svg"} alt="logo"/>
                            <h3 className={"Login__title-text"}>Members log in</h3>
                        </div>
                        <Formik
                            initialValues={{
                                email: "",
                                password: ""
                            }}
                            onSubmit={(values, {setSubmitting}) => {
                                setTimeout(() => {
                                    handleLogin(values.email, values.password)
                                        .then(history.push("./catalog"))
                                        .catch((err) => alert(err));
                                    setSubmitting(false);

                                }, 400);
                            }}
                            validationSchema={loginValidation}
                        >{({
                               values,
                               errors,
                               touched,
                               handleSubmit,
                               isSubmitting,
                           }) => (
                            <Form className={"Login__form"}>
                                <Field
                                    placeholder={"Email"}
                                    className={"Login__input"}
                                    type="email"
                                    name="email"
                                    value={values.email}
                                />
                                {errors.email && touched.email ? alert(errors.email) : null}
                                <Field
                                    placeholder={"Password"}
                                    className={"Login__input"}
                                    type="password"
                                    name="password"
                                    value={values.password}
                                />
                                {errors.password && touched.password ? alert(errors.password) : null}
                                <a href="#" className={"Login__Forgot-pass"}>Forgot Password?</a>
                                <button
                                    className={"Modal__log-in--button"}
                                    type="submit"
                                    onSubmit={handleSubmit}
                                    disabled={isSubmitting}
                                >
                                    Log In
                                </button>
                            </Form>
                        )}
                        </Formik>
                        <a href="#" className={"Modal__log-in-with"}>or log in with</a>
                        <div className={"Login__buttons--wrapper"}>
                            <button
                                className={"Login__buttons"}
                            >
                                <FacebookIcon className={"Login__icons"}/>
                            </button>
                            <button
                                className={"Login__buttons"}
                            >
                                <LinkedInIcon className={"Login__icons"}/>
                            </button>
                        </div>
                        <div className={"Login__sign-up-wrapper"}>
                            <span className={"Login__no-account"}>Don't have an account?</span>
                            <span className={"Login__sign-up"} onClick={() => setSignUpOn(false)}>Sign Up</span>
                        </div>
                    </div>

                    :

                    <div className={"SignUp"}>
                        <div className={"Login__title"}>
                            <img src={"https://app.365dropship.com/assets/images/auth/logo.svg"} alt="logo"/>
                            <h3 className={"Login__title-text"}>Members sign up</h3>
                        </div>
                        <Formik
                            initialValues={{
                                firstName: "",
                                lastName: "",
                                email: "",
                                password: "",
                                passwordConfirmation: "",
                            }}
                            onSubmit={(values, {setSubmitting}) => {
                                setTimeout(() => {
                                    handleSignUp(
                                        values.firstName,
                                        values.lastName,
                                        values.email,
                                        values.password,
                                        values.passwordConfirmation
                                    ).then(history.push("./catalog"))
                                        .catch((err) => alert(err));

                                    setSubmitting(false);
                                }, 400);
                            }}
                        >{({
                               values,
                               errors,
                               touched,
                               handleSubmit,
                               isSubmitting,
                           }) => (
                            <Form className={"Login__form"}>
                                <Field placeholder={"First Name"}
                                       className={"Login__input"}
                                       type="text"
                                       name="firstName"
                                       value={values.firstName}
                                />
                                <Field placeholder={"Last Name"}
                                       className={"Login__input"}
                                       type="text"
                                       name="lastName"
                                       value={values.lastName}
                                />
                                <Field placeholder={"Email"}
                                       className={"Login__input"}
                                       type="email"
                                       name="email"
                                       value={values.email}
                                />
                                <Field placeholder={"Password"}
                                       className={"Login__input"}
                                       type="password"
                                       name="password"
                                       value={values.password}
                                />
                                <Field placeholder={"Confirm Password"}
                                       className={"Login__input"}
                                       type="password"
                                       name="passwordConfirmation"
                                       value={values.passwordConfirmation}
                                />
                                <button className={"Modal__log-in--button"}
                                        type="submit"
                                        onSubmit={handleSubmit}
                                        disabled={isSubmitting}
                                >
                                    Sign up
                                </button>
                            </Form>
                        )}
                        </Formik>
                        <a href="#" className={"Modal__log-in-with"}>or sign up with</a>
                        <div className={"Login__buttons--wrapper"}>
                            <button
                                className={"Login__buttons"}
                            >
                                <FacebookIcon className={"Login__icons"}/>
                            </button>
                            <button
                                className={"Login__buttons"}
                            >
                                <LinkedInIcon className={"Login__icons"}/>
                            </button>
                        </div>
                        <div className={"Login__sign-up-wrapper"}>
                            <span className={"Login__no-account"}>Already have an account?</span>
                            <span className={"Login__sign-up"} onClick={() => setSignUpOn(true)}>Log In</span>
                        </div>
                    </div>
            }
        </>
    );
};

export default Authentication;
