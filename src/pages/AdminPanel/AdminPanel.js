import React from 'react';
import "./AdminPanel.css"
import Aside from "../../components/Aside/Aside";
import AdminProfile from "../../components/AdminProfile/AdminProfile";

const AdminPanel = () => {
    return (
        <>
            <Aside />
            <AdminProfile />
        </>
    );
};

export default AdminPanel;